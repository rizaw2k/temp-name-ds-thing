  Copyright (C) 2005 - 2008
   Michael Noland (joat)
   Jason Rogers (dovoto)
   Dave Murpy (WinterMute)

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any
  damages arising from the use of this software.

  Permission is granted to anyone to use this software for any
  purpose, including commercial applications, and to alter it and
  redistribute it freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you
     must not claim that you wrote the original software. If you use
     this software in a product, an acknowledgment in the product
     documentation would be appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and
     must not be misrepresented as being the original software.
  3. This notice may not be removed or altered from any source
     distribution.


  r4ok Addendum - 2022
	This library has been modified for use and integration 
	with the r4ok kernel, and is not the original work of the
	r4ok development team. All credits go to the original authors
	for their work present in this library, and to the r4ok
	developers for their work in its modification as well.
