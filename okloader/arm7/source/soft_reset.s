@ soft_reset.s
	
	.arch	armv4t
	.cpu	arm7tdmi
	.text
	.align	2
	.global arm7_swi_soft_reset
	.thumb_func
arm7_swi_soft_reset:
	swi	0x00
	.end
